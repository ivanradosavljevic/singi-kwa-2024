# Vežbe 9
*Cilj vežbi: Provera prava pristupa u Angular aplikacijama.*

## Zadaci
1. Napraviti stranicu za prijavu korisnika na sistem.
2. Definisati servis za prijavu i čuvanje podataka o korisniku.
3. Ograničiti prava pristupa delovima aplikacije spram uloga koje poseduje prijavljeni korisnik.