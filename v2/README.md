# Vežbe 2
*Cilj vežbi: Rad sa Angular komponentama.*

## Zadaci
1. Prethodni primer prepraviti tako da svaka logička celina aplikacije bude obuhvaćena jednom komponentom.
2. Prepraviti prikaz nepostojećih vrednosti u tabeli tako da se nepostojeće vrednosti obeležavaju crvenom bojom pozadine.
3. Primeniti prethodno navedeni prikaz nepostojećih vrednosti i na druge elemente u kojima se ove vrednosti mogu pojaviti. Na primer u prikazu pojedinačnog studenta.