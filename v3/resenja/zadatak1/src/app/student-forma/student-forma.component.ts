import { NgFor, NgIf } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ProveraNepostojecihVrednostiDirective } from '../provera-nepostojecih-vrednosti.directive';

@Component({
  selector: 'app-student-forma',
  standalone: true,
  imports: [FormsModule, NgIf, NgFor, ProveraNepostojecihVrednostiDirective],
  templateUrl: './student-forma.component.html',
  styleUrl: './student-forma.component.css'
})
export class StudentFormaComponent {

  @Input()
  smerovi: any[] = [{}, {}];

  @Output()
  studentSubmit: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  student: any = {
    "originalniIndeks": undefined,
    "brojIndeksa": undefined,
    ime: undefined,
    prezime: undefined,
    godinaUpisa: undefined,
    smer:  undefined,
    "prosecnaOcena": undefined
  }

  constructor() { }

  ngOnInit(): void {
  }

  naSubmit() {
    this.studentSubmit.emit({ ...this.student });
  }

  smerTrackBy(indeks: number, smer: any) {
    return smer.sifraSmera;
  }

  resetForme() {
    this.student = {};
  }

  comparator(v1: any, v2: any) {
    if(v1 && v2)
    {
      return v1["sifraSmera"] == v2["sifraSmera"]
    } else {
      return  v1 == v2
    }
  }
}
