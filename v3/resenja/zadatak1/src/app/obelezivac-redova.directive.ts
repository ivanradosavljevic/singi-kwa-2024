import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appObelezivacRedova]',
  standalone: true
})
export class ObelezivacRedovaDirective {
  private originalBackgroundColor: any = "";

  constructor(private element: ElementRef) { }

  @HostListener("mouseenter")
  public onMouseEnter() {
    this.originalBackgroundColor = this.element.nativeElement.style.backgroundColor
    this.element.nativeElement.style.backgroundColor = "lightgray";
  }

  @HostListener("mouseleave")
  public onMouseLeave() {
    this.element.nativeElement.style.backgroundColor = this.originalBackgroundColor;
  }
}
