import { NgFor, NgIf } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-pretraga-studenata',
  standalone: true,
  imports: [FormsModule, NgIf, NgFor],
  templateUrl: './pretraga-studenata.component.html',
  styleUrl: './pretraga-studenata.component.css'
})
export class PretragaStudenataComponent {

  @Input()
  smerovi: any[] = [];

  @Output()
  pretraga: EventEmitter<any> = new EventEmitter<any>();

  parametri: any = {
    "brojIndeksa": undefined,
    ime: undefined,
    prezime: undefined,
    godinaUpisaOd: undefined,
    godinaUpisaDo: undefined,
    smer: undefined,
    "prosecnaOcenaOd": undefined,
    "prosecnaOcenaDo": undefined
  }

  constructor() { }

  ngOnInit(): void {
  }

  smerTrackBy(indeks: number, smer: any) {
    return smer.sifraSmera;
  }

  pretrazi() {
    this.pretraga.emit({ ...this.parametri });
  }
}
