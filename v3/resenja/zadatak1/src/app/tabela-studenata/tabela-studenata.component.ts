import { NgFor, NgIf } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ProveraNepostojecihVrednostiDirective } from '../provera-nepostojecih-vrednosti.directive';
import { ObelezivacRedovaDirective } from '../obelezivac-redova.directive';

@Component({
  selector: 'app-tabela-studenata',
  standalone: true,
  imports: [FormsModule, NgIf, NgFor, ObelezivacRedovaDirective, ProveraNepostojecihVrednostiDirective],
  templateUrl: './tabela-studenata.component.html',
  styleUrl: './tabela-studenata.component.css'
})
export class TabelaStudenataComponent {

  @Input()
  elementi: any[] = [];

  @Output()
  uklanjanje: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  izmena: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  ukloni(i: any, v: any) {
    this.uklanjanje.emit({ index: i, value: { ...v } });
  }

  izmeni(i: any, v: any) {
    this.izmena.emit({ index: i, value: { ...v } });
  }

  sort(v: any) {

  }
}
