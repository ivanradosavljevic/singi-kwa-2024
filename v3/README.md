# Vežbe 3
*Cilj vežbi: Rad sa Angular direktivama.*

## Zadaci
1. Prethodni primer prepraviti tako da se za stilizovanje nepostojećih vrednosti koristi direktiva. Ova direktiva kao ulaz prima promenljivu za koju se vrši provera da li je njena vrednost postavljena. U slučaju da vrednost nije postavljenja direktiva treba da stilizuje element u kojem se vrednost prikazuje tako što će pozadinu elementa i boju teksta u elementu promeniti na boje koje korisnik direktive može postaviti.
2. Napraviti direktivu koja pozadinu reda u tabeli boji sivom bojom ukoliko se kursor nalazi iznad tog reda.
3. Napraviti strukturnu direktivu koja u zavisnosti od toga da li je prosleđen niz elemenata, ili je niz elemenata prazan prikazuje korisnički definisane poruke o greškama ili tabelu.