import { Injectable } from '@angular/core';
import { Smer } from '../model/smer';

@Injectable({
  providedIn: 'root'
})
export class SmeroviService {
  private smerovi : Smer[] = [
    { "sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo" },
    { "sifraSmera": "IT", "naziv": "Informacione tehnologije"},
  ];
  constructor() { }

  dobaviSve() {
    return [...this.smerovi];
  }
}
