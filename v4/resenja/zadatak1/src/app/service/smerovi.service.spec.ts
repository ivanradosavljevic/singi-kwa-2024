import { TestBed } from '@angular/core/testing';

import { SmeroviService } from './smerovi.service';

describe('SmeroviService', () => {
  let service: SmeroviService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmeroviService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
