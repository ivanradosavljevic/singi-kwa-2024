import { NgFor, NgIf } from '@angular/common';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Smer } from '../model/smer';
import { SmeroviService } from '../service/smerovi.service';

@Component({
  selector: 'app-pretraga-studenata',
  standalone: true,
  imports: [FormsModule, NgIf, NgFor],
  templateUrl: './pretraga-studenata.component.html',
  styleUrl: './pretraga-studenata.component.css'
})
export class PretragaStudenataComponent {
  smerovi: Smer[] = [];

  @Output()
  pretraga: EventEmitter<any> = new EventEmitter<any>();

  parametri: any = {
    "brojIndeksa": undefined,
    ime: undefined,
    prezime: undefined,
    godinaUpisaOd: undefined,
    godinaUpisaDo: undefined,
    smer: undefined,
    "prosecnaOcenaOd": undefined,
    "prosecnaOcenaDo": undefined
  }

  constructor(private smeroviServis : SmeroviService) { }

  ngOnInit(): void {
    this.smerovi = this.smeroviServis.dobaviSve();
  }

  smerTrackBy(indeks: number, smer: any) {
    return smer.sifraSmera;
  }

  pretrazi() {
    this.pretraga.emit({ ...this.parametri });
  }
}
