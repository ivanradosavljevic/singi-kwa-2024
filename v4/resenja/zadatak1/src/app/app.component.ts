import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { StudentiService } from './service/studenti.service';
import { TabelaStudenataComponent } from './tabela-studenata/tabela-studenata.component';
import { StudentFormaComponent } from './student-forma/student-forma.component';
import { PretragaStudenataComponent } from './pretraga-studenata/pretraga-studenata.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, TabelaStudenataComponent, StudentFormaComponent, PretragaStudenataComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {

  title = 'Tabela studenata';

  studentZaIzmenu: any = {};
  parametri : any = {};
  filtriraniStudenti: any[] = [];

  constructor(private studentiServis : StudentiService) {
    this.filtriraniStudenti = studentiServis.pretraziStudente();
  }

  postaviZaIzmenu(red: any) {
    this.studentZaIzmenu = { ...red.value, originalniIndeks: red.value["brojIndeksa"] };
  }

  obradaStudenta(student: any) {
    this.studentiServis.azuriranje(student);
    this.pretraziStudente(this.parametri);
  }

  ukloni(ukloniDogadjaj: any) {
    this.studentiServis.ukloni(ukloniDogadjaj);
    this.pretraziStudente(this.parametri);
  }

  pretraziStudente(parametri : any) {
    if(parametri === undefined) {
      parametri = this.parametri;
    } else {
      this.parametri = parametri;
    }
    this.filtriraniStudenti = this.studentiServis.pretraziStudente(parametri);
  }
}
